#!/usr/bin/python3

import os
import shutil

class bc:
    HEAD = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    LINE = '\033[4m'



print ('\n{}[ WARNING ]:{}\n'
  .format (bc.BOLD+bc.WARN, bc.END))

val = input ('Do you want to delete all cached data? [type YES]: ')


if not val == 'YES':
  print ('\n{}Quitting...{}'
    .format (bc.BOLD+bc.GREEN, bc.END))
  quit ()


# deletes cache directories
for directory in ['users', 'entries', 'matches']:
  if os.path.isdir (directory):
    shutil.rmtree (directory)




