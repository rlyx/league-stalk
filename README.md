## League Stalk

A small and simple script to check if your friends are playing league (as someone who doesn't play league).

This was originally meant to be a joke but I figured I might as well release it anyway.

There are still parts of the script that I need to refactor.


### Prerequisites
- Have **Python 3** installed before running the script.
- You need to have a Riot Games account (https://riotgames.com/).
- You must vist the developer portal (https://developer.riotgames.com/).
- From there, you can generate / regenerate the API key needed for this script.


### Running the script
0. Copy the API key you got from https://developer.riotgames.com/ into `key.txt`.
1. Add usernames to `users.txt` **(one per line)**.
2. Run `python3 check.py`.


### Settings
- `settings.txt` contains some of the more important settings that one might need to change before running the script.
  - `REGION` : The world region of the accounts. Ex: na1, kr, jp1, ru1, etc. (see [**this**](https://leagueoflegends.fandom.com/wiki/Servers) for all valid values)
  - `REFRESH_HOUR` : How often (in hours) user data get updated.

### Deleting the cache
0. Run `python3 delete_cache.py` to delete all the generated folders and their contents (or just delete them yourself).
