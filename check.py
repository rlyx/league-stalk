#!/usr/bin/python3

import os
import time
import json
import glob
import random
import requests
import configparser
from datetime import datetime, timedelta


KEY = ''
KEYFILE = 'key.txt'
USERFILE = 'users.txt'
SETTINGSFILE = 'settings.txt'
REGION = 'na1'
ROUTE = 'americas'
REFRESH_HOUR = 1
TFT_GAME_HOUR = 24

# update settings from settings file
if os.path.isfile (SETTINGSFILE):
  config = configparser.ConfigParser ()
  config.read (SETTINGSFILE)

  settings = config['global']
  KEYFILE = settings['KEYFILE']
  USERFILE = settings['USERFILE']
  DOMAIN = settings['DOMAIN'].lower ()
  REGION = settings['REGION'].lower ()
  ROUTE = settings['ROUTE'].lower ()
  REFRESH_HOUR = int (settings['REFRESH_HOUR'])
  TFT_GAME_HOUR = int (settings['TFT_GAME_HOUR'])


URL = 'https://' + REGION + '.' + DOMAIN
URL_TFT = 'https://' + ROUTE + '.' + DOMAIN
REFRESH_TIME = REFRESH_HOUR * 60 * 60
THRESHOLD_TIME = TFT_GAME_HOUR * 60 * 60


class api:
  user = URL + '/lol/summoner/v4/summoners/by-name/'
  entry = URL + '/lol/league/v4/entries/by-summoner/'
  match = URL + '/lol/spectator/v4/active-games/by-summoner/'
  list_tft = URL_TFT + '/tft/match/v1/matches/by-puuid/'
  match_tft = URL_TFT + '/tft/match/v1/matches/'
  
class rank:
  rs = 'RANKED_SOLO_5x5'
  rf = 'RANKED_FLEX_SR'
  tft = 'RANKED_TFT_PAIRS'

class errmsg:
  null = 'ERROR: query object is null.'
  generic = 'ERROR on {}: {}'
  keyfile = 'ERROR: key.txt is empty. Please paste key into file.'
  expired = 'ERROR: request rejected (403). Key might be expired.'
  notfound = 'ERROR: {} was not found.'

class bc:
    HEAD = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    LINE = '\033[4m'
    JUMP = '\033[0m\033[1m'



def getFile (filename):
  if os.path.isfile (filename):
    return open (filename, 'r')
  else:
    print (errmsg.notfound
      .format (filename))
    quit ()


def archiveFile (path, minimumFileAge):
  fileTime = int (os.path.getctime (path))
  currentTime = int (time.time ())
  fileAge = (currentTime - fileTime)
  
  if fileAge >= int (minimumFileAge):
    if os.path.exists (path + '.old'):
      os.remove (path + '.old')          
    os.rename (path, path + '.old')


def dumpJson (query, filePath):
  try:
    if query.status_code == 200:
      with open (filePath, 'w', encoding='utf-8') as f:
        json.dump (query.json (), f, ensure_ascii=False, indent=2)
    elif query.status_code == 403:
      print (errmsg.expired)
    else:
      print (errmsg.generic
        .format (filePath, query.status_code))
      print (query.json ())
  except AttributeError:
    print (errmsg.null)


# clear terminal window
os.system ('cls || clear')


# create user & matches directory if not present
for directory in ['users', 'entries', 'matches']:
  if not os.path.isdir (directory):
    os.mkdir (directory)


# try to grab API key from key.txt
with getFile (KEYFILE) as keyfile:
  KEY = keyfile.readline ().strip ()
  if not KEY:
    print (errmsg.keyfile)
    quit ()
  else:
    print ('Using key: [{}]\n'
      .format (KEYFILE))


# grab and store user ids as users/<username>.json
with getFile (USERFILE) as users:
  for user in users:
    user = user.strip ()
    userJsonPath = 'users/' + user + '.json' 
    
    if os.path.isfile (userJsonPath):
      archiveFile (userJsonPath, REFRESH_TIME)

    if not os.path.isfile (userJsonPath):
      query = requests.get (api.user + user + '?api_key=' + KEY)
      dumpJson (query, userJsonPath)


# grab and store user stats (entries) as entries/<username>.json
print ('Grabbing user stats:\n')
print (bc.BOLD+bc.LINE)
print ('[ {:18s} | {:12s} | {:12s} | {:6s} ]'
  .format ('Username:', 'Ranked Solo:', 'Ranked Flex:', 'Level:'))
print (bc.END, end='')
for filename in glob.iglob (f'users/*.json'):
  with open (filename, 'r') as u:
    user = json.load (u)
    name = user['name'].strip ()
    id = user['id'].strip ()
    level = str (user['summonerLevel'])
    
    entryJsonPath = 'entries/' + name + '.json'

    if os.path.isfile (entryJsonPath):
      archiveFile (entryJsonPath, REFRESH_TIME)

    if not os.path.isfile (entryJsonPath):
      query = requests.get (api.entry + id + '?api_key=' + KEY)
      dumpJson (query, entryJsonPath)
      
    with open (entryJsonPath, 'r') as e:
      entries = json.load (e)
      
      solo = "Unranked"
      flex = "Unranked"
      
      for entry in entries:
        rankType = entry['queueType']
        
        if rankType == rank.rs:
          solo = entry['tier'] + ' ' + entry['rank']
        elif rankType == rank.rf:
          flex = entry['tier'] + ' ' + entry['rank']
    
    print ('[ {:18s} | {:>12s} | {:>12s} | {:>6s} ]'
      .format (name, solo, flex, level))
        
print ('\n')


# grab active matches per user
for filename in glob.iglob (f'users/*.json'):
  
  with open (filename, 'r') as f:
    data = json.load (f)
    name = data['name'].strip ()
    id = data['id'].strip ()
    
    query = requests.get (api.match + id + '?api_key=' + KEY)
    
    match = query.json ()
    status = 'NOT IN GAME'

    try:
      if query.status_code == 200:
        status = 'IN GAME'
        gameId = str (match['gameId'])
        dumpJson (query, 'matches/' + gameId + '.json')

        gameMode = match['gameMode']
        gameType = match['gameType'].split('_')[0]
        
        random.seed (int (match['gameId']))
        idColour = '\033[' + str (random.randint (91, 96)) + 'm'
        
        if match['gameLength'] < 0:
          gameLength = str (match['gameLength']) + ' seconds.'
        else:
          gameLength = str (
            timedelta (seconds=match['gameLength'])
          )
        
        if match['gameStartTime'] < 1:
          gameStart = 'STARTING...'
        else:
          gameStart = datetime.fromtimestamp (
            match['gameStartTime'] / 1000
          ).strftime (
            '%y-%m-%d %H:%M:%S'
          )

        print ('{}[ {:18s} | {:>12s} | {}#{:>11s}{} ]{}'
          .format (
            bc.BOLD+bc.GREEN, name, status, idColour,
            gameId, bc.JUMP, bc.END
          ))
          
        print (
          '{}[ {:18s} | {:12s} | {:12s} | {:>7s} ]{}\n'
          .format (
            bc.BOLD, gameStart, gameLength, gameType,
            gameMode, bc.END
          ))
        
      elif query.status_code == 404:
        print ('{}[ {:18s} | {:>12s} ]{}\n'
          .format (
            bc.BOLD+bc.WARN, name, status, bc.END
          ))
      else:
        print (errmsg.generic
          .format (name, query.status_code))
        print (query.json ())
    except AttributeError:
      print (errmsg.null)


# grab last TFT match per user
print ('\nLatest TFT Game (if applicable):\n')

for filename in glob.iglob (f'users/*.json'):
  
  with open (filename, 'r') as f:
    data = json.load (f)
    name = data['name'].strip ()
    puuid = data['puuid'].strip ()
    
    query = requests.get (
      api.list_tft + puuid + '/ids?count=1&api_key=' + KEY)

    try:
      if len (query.json ()) == 1:
        matchId = str (query.json ()[0])
        
        print ('{}[ {:18s} | {:14s} ]{}'
          .format (
            bc.BOLD+bc.WARN, name, matchId, bc.END
          ))
        
        query = requests.get (
          api.match_tft + matchId + '?api_key=' + KEY)
          
        match = query.json ()['info']
        
        gameStart = datetime.fromtimestamp (
          match['game_datetime'] / 1000
        ).strftime (
          '%y-%m-%d %H:%M:%S'
        )
        
        gameLength = str (
          timedelta (seconds=match['game_length'])
        )
        
        currentTimestamp = int (time.time ())
        gameStartTimestamp = int (match['game_datetime'] / 1000)
        timeDiff = currentTimestamp - gameStartTimestamp
        
        if timeDiff <= THRESHOLD_TIME:
          colour = bc.BOLD + bc.GREEN
        else:
          colour = bc.BOLD
        
        gameType = str (match.get ('tft_game_type', None))
        gameSetName = str (match.get ('tft_set_core_name', None))
        
        print ('{}[ {:18s} | {:14s} | {:8s} | {:>10s} ]{}\n'
          .format (
            colour, gameStart, gameLength,
            gameType, gameSetName, bc.END
          ))
        
    except AttributeError:
      print (errmsg.null)



